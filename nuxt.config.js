//const { API_ROOT, I18N } = require('./config')
module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'KMALEON',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.png' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Google+Sans:400,500|Material+Icons' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Poppins:300,400,600,700,900&amp;subset=latin-ext' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/icon?family=Material+Icons' },
      { rel: 'stylesheet', href: 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css'}
    ],
    /*
    css: [
      'hover.css/css/hover-min.css',
      'bulma/css/bulma.css',
      "HTML_design/libs/fancybox/jquery.fancybox.css",
      "HTML_design/libs/owl-carousel/assets/owl.carousel.css",
      "HTML_design/style.css",
      {src: "~/assets/css/style.css", lang: 'css'}
    ],
    */
  },
  /*
  ** Customize the progress bar color
  */
  loading: { color: 'red' },
  /*
  ** Build configuration
  */
  build: {
    /*vendor: ['vue-i18n'], // webpack vue-i18n.bundle.js*/

    /*
    ** Run ESLint on save
    */
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  },
  router: {              // customize nuxt.js router (vue-router).
    middleware: 'i18n'   // middleware all pages of the application
  },
  plugins: ['~/plugins/i18n.js', '~/plugins/vue-carousel'], // webpack plugin
  // plugins: [{ src: '~plugins/vue-carousel', ssr: false }],

}
